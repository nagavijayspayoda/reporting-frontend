# README #

This is **work in progress** reporting frontend component built using Angular 5.

### What is this repository for? ###

* This is reusable UI library component for building reports for any angular based web applications.

## To Run 

Run `ng serve` on command prompt. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
