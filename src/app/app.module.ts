import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';


import { AppComponent } from './app.component';
import { CorsInterceptor} from './cors.interceptor';
import {HttpClientModule} from '@angular/common/http';

import { AmexioWidgetModule,CommonHttpService } from 'amexio-ng-extensions';


@NgModule({
  declarations: [
    AppComponent
    
  ],
  imports: [
    BrowserModule,
    AmexioWidgetModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [CommonHttpService, { provide: HTTP_INTERCEPTORS, useClass: CorsInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
