import { ChangeDetectorRef,Component,OnInit } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  //tableName: string;
  baseFieldUrl: string = "http://localhost:9000/v1/fields";
  tables: Table[];
  fieldsCheckbox : any;
  fieldUrl: string;
  constructor(private http: HttpClient) {
    this.tables = [];
    // this.fieldsCheckbox = [];
    this.fieldUrl = 'http://localhost:9000/v1/fields' ;
  }
  ngOnInit() {
    // this.http.get('http://localhost:9000/v1/tablesnew').subscribe(response => {
    //   this.tables = response['data'];
    // });
    //this.fieldsCheckbox = {"data":  [{"fieldName":"Order Id","fieldId": "1"}]};
  }
  // writeValue(value: any) {
  //   if (value !== undefined) {
  //     this.fieldsCheckbox = value;
  //   }
  // }
  // registerOnChange() {

  // }
  // registerOnTouched() {

  // }
  getSingleVal(value : any){
   console.log(value['tableId']);
   this.fieldUrl = this.baseFieldUrl + "?tableName=" + value['tableId'];
   console.log(this.fieldUrl);
   this.http.get(this.fieldUrl).subscribe(response => {
    this.fieldsCheckbox = response;
    //  let fieldData =  response['data'];
    //  fieldData.forEach(element => {
    //   this.fieldsCheckbox.push(element);
    //  });
     //this.fieldsCheckbox.push()
     console.log("Data:::::" + JSON.stringify(this.fieldsCheckbox));
    });
   
  }

  saveReportConfig() {
    
  }
}
interface Table {
  tableName: string,
  tableId: string
}
